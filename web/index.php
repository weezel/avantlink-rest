<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JDesrosiers\Silex\Provider\CorsServiceProvider;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

//CORS
$app->register(new CorsServiceProvider());

//DB Connection
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => 'avantlinktest.db.6123544.hostedresource.com',
        'dbname' => 'avantlinktest',
        'user' => 'avantlinktest',
        'password' => 'vR6qZanB#3@mT3A!',
        'charset' => 'utf8',
    ),
));

//Set to accept JSON requests.
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

//Error logging.
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../logs/errors.log',
));

//---- Routes ----

//Default
$app->get('/', function () use ($app) {
    return $app->json("This is not the page you are looking for... move along.");
});

//Get all tasks.
$app->get('/api/tasks', function () use ($app) {
    $sql = "SELECT * FROM task";
    $tasks = $app['db']->fetchAll($sql);

    return $app->json($tasks);
});

//Get task by ID.
$app->get('/api/tasks/{id}', function ($id) use ($app) {
    $sql = "SELECT * FROM task WHERE id = ?";
    $task = $app['db']->fetchAssoc($sql, array((int) $id));

    return $app->json($task);
});

//Add new task.
$app->post('/api/tasks', function (Request $request) use ($app) {
    $insertArray = array(
        'task_name' => $request->get('task_name'),
        'task_desc' => $request->get('task_desc')
    );
    $app['db']->insert('task', $insertArray);
    //Get last ID to send back.
    $insertArray['id'] = $app['db']->lastInsertId();

    return $app->json($insertArray);
});

//Delete task by ID.
$app->delete('/api/tasks/{id}', function ($id) use ($app) {
    $app['db']->delete('task', array(
        'id' => $id
    ));
    return new Response($id);
});

//---- End Routes ----


//Error handling.
$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    switch ($code) {
        case 404:
            $message = 'The requested page could not be found.';
            break;
        default:
            $message = 'Sorry! Error code ' . $code . ", " . $e->getMessage();
    }
    
    $app['monolog']->addInfo(sprintf("Error: ", $message));

    return new Response($message);
});

//Enable CORS
$app->after($app["cors"]);

$app->run();
